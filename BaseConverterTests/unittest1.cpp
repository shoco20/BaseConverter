#include "stdafx.h"
#include "CppUnitTest.h"
#include "../ProjectBaseC/ProjectBaseC.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace BaseConverterTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(convertDigitToNumber_test)
		{
			Assert::AreEqual(15, convertDigitToNumber('F'));
			Assert::AreEqual(10, convertDigitToNumber('A'));
			Assert::AreEqual(7, convertDigitToNumber('7'));
			Assert::AreEqual(0, convertDigitToNumber('0'));
			Assert::AreEqual(-1, convertDigitToNumber('='));
		}

		TEST_METHOD(isNumOkay_test)
		{
			Assert::IsTrue(isNumOkay("253", 6));
			Assert::IsTrue(isNumOkay("DEAD", 16));
			Assert::IsTrue(isNumOkay("DEAD.BEEF", 16));
			Assert::IsFalse(isNumOkay("561", 2));
			Assert::IsFalse(isNumOkay("10", -2));
			Assert::IsFalse(isNumOkay("", 4));
			Assert::IsFalse(isNumOkay("241.423.5", 9));
			Assert::IsFalse(isNumOkay("123,56,1", 8));
		}	
		
		TEST_METHOD(convertNumberToDigit_test)
		{
			Assert::AreEqual('0', convertNumberToDigit(0));
			Assert::AreEqual('7', convertNumberToDigit(7));
			Assert::AreEqual('A', convertNumberToDigit(10));
			Assert::AreEqual('F', convertNumberToDigit(15));
		}
		
		TEST_METHOD(sum_Test)
		{
			Assert::AreEqual("224", sum("125", "99", 10).c_str());
			Assert::AreEqual("10000", sum("1001", "111", 2).c_str());
			Assert::AreEqual("1", sum("1", "0", 2).c_str());
		}

		TEST_METHOD(multHelper_Test)
		{
			Assert::AreEqual("45", multHelper("9", "5", 10).c_str());
			Assert::AreEqual("E1", multHelper("F", "F", 16).c_str());
			Assert::AreEqual("10", multHelper("10", "1", 2).c_str());
			Assert::AreEqual("0", multHelper("0", "25", 10).c_str());
		}

		TEST_METHOD(mult_Test)
		{
			Assert::AreEqual("4590", mult("90", "51", 10).c_str());
			Assert::AreEqual("7D0", mult("8", "FA", 16).c_str());
			Assert::AreEqual("11010100100", mult("100010", "110010", 2).c_str());
			Assert::AreEqual("0", mult("0", "2251", 10).c_str());
		}

		TEST_METHOD(pow_Test)
		{
			Assert::AreEqual("32", pow("2", 5, 10).c_str());
			Assert::AreEqual("10000", pow("10", 4, 2).c_str());
			Assert::AreEqual("3E8", pow("A", 3, 16).c_str());
		}
		
		TEST_METHOD(convertHelper_Test)
		{
			Assert::AreEqual("8", convertHelper("1000", 2, 10).c_str());
			Assert::AreEqual("1010", convertHelper("10", 10, 2).c_str());
			Assert::AreEqual("9", convertHelper("1001", 2, 16).c_str());
		}

		TEST_METHOD(convertInteger_Test)
		{
			Assert::AreEqual("24251", convertInteger("10100010101001", 2, 8).c_str());
			Assert::AreEqual("33220100", convertInteger("FA10", 16, 4).c_str());
			Assert::AreEqual("4321341", convertInteger("73346", 10, 5).c_str());
		}
		
		TEST_METHOD(convertFraction_Test)
		{
			Assert::AreEqual(".001", convertFraction("125", 10, 2).c_str());
			Assert::AreEqual(".1311", convertFraction("75", 16, 4).c_str());
			Assert::AreEqual(".15625", convertFraction("00101", 2, 10).c_str());
		}

		TEST_METHOD(getNumPart_Test)
		{
			Assert::AreEqual("25", getNumPart("25.97", false).c_str());
			Assert::AreEqual("97", getNumPart("25.97", true).c_str());

			Assert::AreEqual("", getNumPart(".12", false).c_str());
			Assert::AreEqual("12", getNumPart(".12", true).c_str());

			Assert::AreEqual("999", getNumPart("999", false).c_str());
			Assert::AreEqual("", getNumPart("999", true).c_str());
		}

		TEST_METHOD(fullConvert_Test)
		{
			Assert::AreEqual("2E9", fullConvert("521", 12, 16).c_str());
			Assert::AreEqual("-120", fullConvert("-F", 17, 3).c_str());
			Assert::AreEqual("10000.01", fullConvert("16.25", 10, 2).c_str());
		}
	};
}