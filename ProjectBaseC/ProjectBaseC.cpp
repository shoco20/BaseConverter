// ProjectBaseC.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <C:\pdcurs36\curses.h>
using namespace std;

char *choices_main[] = {
	"Base converter", "Authors", "Exit"
};
int n_choices_main = sizeof(choices_main) / sizeof(char*);

char *choices_convert[] = {
	"Number: ", "From base: ", "To base: ", "Convert...", "Back"
};
int n_choices_convert = sizeof(choices_convert) / sizeof(char*);

char convertNumberToDigit(int num)
{
	if (num < 10)
		return num + '0';
	else
		return num + 'A' - 10;
}

int convertDigitToNumber(char ch)
{
	if (ch >= '0' && ch <= '9')
	{
		return ch - '0';
	}
	else if (ch >= 'A')
	{
		return ch - 'A' + 10;
	}
	else
		return -1;
}

bool isNumOkay(string number, int base)
{
	if (number.empty()) return false;

	if (number[0] == '+' || number[0] == '-')
		number.erase(0, 1);

	int delim_count = 0;
	for (unsigned int i = 0; i < number.length(); i++)
	{
		if (convertDigitToNumber(number[i]) >= base)
			return false;

		if (convertDigitToNumber(number[i]) == -1)
		{
			if (number[i] == '.' || number[i] == ',')
				delim_count++;
			else
				return false;
		}
	}

	if (delim_count > 1)
		return false;
	else
		return true;
}

string sum(string a, string b, int base = 10)
{
	if (a.size() < b.size())
		swap(a, b);

	b.insert(0, a.size() - b.size(), '0');

	for (size_t i = 0; i < a.length(); i++)
	{
		a[i] = convertNumberToDigit(convertDigitToNumber(a[i]) + convertDigitToNumber(b[i]));
	}

	a.insert(0, 1, '0');
	for (int i = a.length() - 1; i >= 0; i--)
	{
		if (i != 0) a[i - 1] = convertNumberToDigit(convertDigitToNumber(a[i]) / base + convertDigitToNumber(a[i - 1]));

		a[i] = convertNumberToDigit(convertDigitToNumber(a[i]) % base);

	}

	while (a[0] == '0' && a.size() > 1) a.erase(0, 1);

	return a;
}

string multHelper(string a, string b, int base = 10)
{
	if (a.size() < b.size()) swap(a, b);

	string result = "0";
	string count = "0";

	if (a == "0" || b == "0") return "0";

	while (b != count) {
		result = sum(result, a, base);
		count = sum(count, "1", base);
	}

	return result;
}

string mult(string a, string b, int base = 10)
{
	string result = "0";
	string mult_result;

	for (size_t i = 0; i < b.size(); i++)
	{
		mult_result = multHelper(a, string(1, b[b.size() - 1 - i]), base);
		mult_result.insert(mult_result.end(), i, '0');
		result = sum(result, mult_result, base);
	}

	return result;
}

string pow(string num, int power, int base = 10)
{
	string result = num;

	if (power == 0) return "1";

	while (power > 1)
	{
		result = mult(result, num, base);

		power--;
	}

	return result;
}

string convertHelper(string num, int base1, int base2)
{
	string result = "";
	int number = 0;

	for (int i = num.length() - 1; i >= 0; i--)
	{
		number += (convertDigitToNumber(num[i]) * int(pow(base1, num.length() - 1 - i)));
	}

	int remainder = 0;

	if (number == 0) return "0";

	while (number > 0)
	{
		remainder = number % base2;
		result.insert(result.begin(), convertNumberToDigit(remainder));
		number = number / base2;
	}

	return result;
}

string convertInteger(string a, int base1, int base2)
{
	string power, digit, multiplication;
	string result = "0";
	string old_base_in_new_base = convertHelper(to_string(base1), 10, base2);

	for (int i = 0; i < int(a.length()); i++)
	{
		power = pow(old_base_in_new_base, (a.length() - 1 - i), base2);
		digit = convertHelper(string(1, a[i]), base1, base2);
		multiplication = mult(digit, power, base2);
		result = sum(result, multiplication, base2);
	}

	return result;
}

string convertFraction(string fraction, int from_base, int to_base, int precision = 10)
{
	string new_fraction = "";

	double fraction_part = 0.0;
	for (int digit = 0, index = 0; index < int(fraction.length()); index++)
	{
		digit = convertDigitToNumber(fraction[index]);

		if (digit != 0)
			fraction_part += digit * pow(from_base, -1 * (index + 1));
	}

	if (fraction_part != 0.0) new_fraction.push_back('.');

	int i = 0;
	for (double f = fraction_part * to_base; f > 0.01f && i < precision; i++)
	{
		new_fraction.push_back(convertNumberToDigit(int(f)));
		f -= int(f);
		f *= to_base;
	}

	return new_fraction;
}

string getNumPart(string str, bool getFraction)
{
	bool hasFraction = false;

	int delim_place = 0;
	for (unsigned int i = 0; i < str.length(); i++)
	{
		if (str[i] == '.' || str[i] == ',')
		{
			delim_place = i;
			hasFraction = true;
			break;
		}
	}

	if (hasFraction && getFraction)
	{
		return str.substr(delim_place + 1, str.length());
	}
	else if (hasFraction && !getFraction)
	{
		return str.substr(0, delim_place);
	}
	else if (!hasFraction && !getFraction)
	{
		return str;
	}
	else
	{
		return "";
	}
}

string fullConvert(string number, int from_base, int to_base, int precision = 10)
{
	transform(number.begin(), number.end(), number.begin(), ::toupper);

	string integer = getNumPart(number, false);
	string fraction = getNumPart(number, true);

	string new_integer = "";
	string new_fraction = "";
	string final_number = "";

	bool is_negative = false;

	if (integer[0] == '-')
	{
		is_negative = true;
		integer.erase(0, 1);
	}
	else if (integer[0] == '+')
	{
		is_negative = false;
		integer.erase(0, 1);
	}

	new_integer = convertInteger(integer, from_base, to_base);
	new_fraction = convertFraction(fraction, from_base, to_base, precision);

	final_number.append(new_integer);
	final_number.append(new_fraction);

	if (is_negative) final_number.insert(0, 1, '-');

	return final_number;
}


void showInformationWindow(vector <string> text, bool error_message = false)
{
	scr_dump("bobby.save");

	WINDOW* win;

	int width = getmaxx(stdscr) * 4 / 5;
	int height = getmaxy(stdscr) * 2 / 3;
	int x = getmaxx(stdscr) / 2 - width / 2;
	int y = getmaxy(stdscr) / 2 - height / 2;

	unsigned int max_string_length = width - 2;

	win = newwin(height, width, y, x);

	init_color(COLOR_RED, 153 * 3, 31, 0);

	if (error_message) {
		init_pair(42, COLOR_RED, COLOR_WHITE);
	}
	else {
		init_pair(42, COLOR_BLUE, COLOR_WHITE);
	}

	wbkgd(win, COLOR_PAIR(42));

	box(win, 0, 0);

	if (error_message) {
		mvwprintw(win, 0, width / 2 - 3, "Error!");
	}
	else {
		mvwprintw(win, 0, width / 2 - 3, "Info");
	}

	vector<string> new_vector;
	for (size_t i = 0; i < text.size(); i++)
	{
		if (text[i].length() > max_string_length) {

			int len = text[i].length() / max_string_length;

			int j = len;
			while (j >= 0) {
				new_vector.push_back(string(text[i].substr((len - j) * max_string_length, max_string_length)));
				j--;
			}
		}
		else {
			new_vector.push_back(text[i]);
		}
	}

	for (size_t i = 0; i < new_vector.size(); i++)
	{
		mvwprintw(win, 2 + 2 * i, width / 2 - new_vector[i].length() / 2, new_vector[i].c_str());
	}

	wrefresh(win);
	wgetch(win);
	delwin(win);

	scr_restore("bobby.save");
}

void showMenu(WINDOW* win, int ch, char* choices[], int n_choices, int &highlight)
{
	if (ch == KEY_UP) highlight--;
	else if (ch == KEY_DOWN) highlight++;


	if (highlight < 0) highlight = n_choices - 1;
	highlight %= n_choices;

	for (int i = 0; i < n_choices; i++)
	{
		if (i == highlight) {
			wattron(win, A_REVERSE);
			mvwprintw(win, 2 * i + 2, 2, choices[i]);
			wattroff(win, A_REVERSE);
		}
		else {
			mvwprintw(win, 2 * i + 2, 2, choices[i]);
		}
	}

	wrefresh(win);
}


void redrawConvertMenu(WINDOW* convert_menu)
{
	init_pair(1, COLOR_GREEN, COLOR_BLACK);

	mvwvline(convert_menu, 0, 20, 0, getmaxy(convert_menu));

	for (int i = 0; i < 3; i++)
	{
		mvwhline(convert_menu, 3 + 2 * i, 22, 0, 27);
	}

	box(convert_menu, 0, 0);
	mvwprintw(convert_menu, 0, 1, "Converter Window");

}

void loadBaseConverterMenu()
{
	scr_dump("ricky.save");

	WINDOW* convert_menu;
	convert_menu = newwin(getmaxy(stdscr) - 1, getmaxx(stdscr) / 3 * 2 + 2, 1, getmaxx(stdscr) / 3);
	redrawConvertMenu(convert_menu);
	keypad(convert_menu, TRUE);

	vector<string> text;

	const int size = 100;
	char number[size], from_base[size], to_base[size];
	number[0] = '\0'; from_base[0] = '\0'; to_base[0] = '\0';

	vector<char*> data;
	data.push_back(number);
	data.push_back(from_base);
	data.push_back(to_base);

	int highlight = 0;
	int ch = 0;
	bool exit = false;
	while (!exit)
	{
		ch = getch();
		showMenu(convert_menu, ch, choices_convert, n_choices_convert, highlight);

		if (ch == 27 || ch == KEY_LEFT) { exit = true; break; }
		if (ch == 10 || ch == KEY_RIGHT) {

			if (highlight < 3)
			{
				curs_set(1);
				echo();
				wmove(convert_menu, 2 + 2 * highlight, strlen(choices_convert[0]) + 10);

				wclrtoeol(convert_menu);
				redrawConvertMenu(convert_menu);

				mvwgetnstr(convert_menu, 2 + 2 * highlight, strlen(choices_convert[0]) + 15, data[highlight], size);
				noecho();

				redrawConvertMenu(convert_menu);
				curs_set(0);
			}
			else if (highlight == 3)
			{
				text.clear();
				string str = "";
				string num = data[0];
				transform(num.begin(), num.end(), num.begin(), ::toupper);

				if (atoi(data[1]) < 2 || atoi(data[1]) > 36 || atoi(data[2]) < 2 || atoi(data[2]) > 36) {
					text.push_back("Wrong input!");
					text.push_back("The base number must not be less than 2 or more than 36!");
					showInformationWindow(text, true);
				}
				else if (!isNumOkay(num, atoi(data[1])))
				{
					text.push_back("Incorrect number!");
					text.push_back("The number must not consist of digits...");
					text.push_back("...that greater than or equal to the base of the number!");
					showInformationWindow(text, true);
				}
				else {
					str = "";
					str.append(fullConvert(num, atoi(data[1]), atoi(data[2])));
					text.push_back(str);
					showInformationWindow(text);
				}
			}
			else if (highlight == 4)
			{
				exit = true;
			}
		}
	}
	wrefresh(convert_menu);
	delwin(convert_menu);
	scr_restore("ricky.save");
}

int cui()
{
	initscr();
	curs_set(0);
	clear();
	noecho();
	cbreak();
	nodelay(stdscr, TRUE);
	keypad(stdscr, TRUE);
	start_color();
	refresh();

	WINDOW* main_menu;
	main_menu = newwin(getmaxy(stdscr) - 1, getmaxx(stdscr) / 3, 1, 0);
	box(main_menu, 0, 0);
	mvwprintw(main_menu, 0, 1, "Main menu");

	vector<string> text;
	int highlight = 0;
	int ch = 0;
	bool exit = false;
	while (!exit)
	{
		ch = getch();

		showMenu(main_menu, ch, choices_main, n_choices_main, highlight);
		if (ch == 10 || ch == KEY_RIGHT)
		{
			switch (highlight) {
			case 0:
				loadBaseConverterMenu();
				break;
			case 1:
				text.clear();
				text.push_back("Programmers: Johnny Poo, Victor Cook");
				showInformationWindow(text);
				break;
			case 2:
				exit = true;
				break;
			}
		}
		else if (ch == 27)
		{
			exit = true;
			break;
		}

	}

	delwin(main_menu);
	refresh();
	endwin();
	return 0;
}

int main(int argc, char** argv)
{
	string num;
	int a, b;
	if (argc > 1 && !strcmp(argv[1], "console"))
	{
		cin >> num >> a >> b;
		cout << fullConvert(num, a, b) << endl;
	}
	else
		cui();

	return 0;
}
